# This Makefile was heavily inspired by the Makefile of the package git2r
# (Version: 0.18.0.9000)

# Determine package name and version from DESCRIPTION file
PKG_VERSION=$(shell grep -i ^version DESCRIPTION | cut -d : -d \  -f 2)
PKG_NAME=$(shell grep -i ^package DESCRIPTION | cut -d : -d \  -f 2)

# Name of built package
PKG_TAR=$(PKG_NAME)_$(PKG_VERSION).tar.gz

# Build documentation with roxygen
# 1) Remove old doc
# 2) Generate documentation
roxygen:
	rm -f man/*.Rd
	cd .. && Rscript -e "library(devtools); document('$(PKG_NAME)')"

# Install package
install:
	cd .. && R CMD INSTALL $(PKG_NAME)

# Generate PDF output from the Rd sources
# 1) Rebuild documentation with roxygen
# 2) Generate pdf, overwrites output file if it exists
pdf: roxygen
	cd .. && R CMD Rd2pdf --force $(PKG_NAME)

# Build and check package
check:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && _R_CHECK_CRAN_INCOMING_=FALSE NOT_CRAN=true \
        R CMD check $(PKG_TAR)

check-no-vignette:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --no-vignettes --no-build-vignettes $(PKG_TAR)

check-as-cran:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --as-cran $(PKG_TAR)

# test generating from raw-data
raw-data:
	Rscript -e 'library(katzar);webpage <- "https://www.hethport.uni-wuerzburg.de/katzas/Katalog.php";katzar:::katzengraben(webpage);sf::st_write(katzas, "katzas.gml", delete_dsn = TRUE)'
	Rscript -e 'library(katzar); schuss <- katzas; katzar:::hexen(schuss);sf::st_write(katzar, "katzar.gml", delete_dsn = TRUE)'

serve: rendering
	jekyll serve -s public/ -d public/_site/

rendering:
	Rscript -e 'library(devtools); devtools::build_readme();  devtools::build_site()'

