<!-- README.md is generated from README.Rmd. Please edit that file -->
<!-- badges: start -->

[![pipeline
status](https://gitlab.com/nehemie/katzar/badges/main/pipeline.svg)](https://gitlab.com/nehemie/katzar/-/commits/main)
<!-- badges: end -->

This R-package is designed to facilitate the import and use of *Katalog
zentralanatolischer Siedlungen des 2. und vorhellenistischen 1.
Jahrtausends v. Chr. in neueren Survey-Berichten (KatzaS)* created by
Dietrich Sürenhagen (Heidelberg):
[&lt;hethiter.net/:KatzaS&gt;](https://www.hethport.uni-wuerzburg.de/katzas/Katalog.php).
It is catalog of archaeological sites discovered during archaeological
surveys in the center regions of Turkey. They are described in the
literature and were aligned by Dietrich Sürenhagen (Heidelberg) The
data-set contains 1335 Central Anatolian sites from 2000 BCE to 300 BCE
corresponding to the periods from the Middle Bronze Age to the end of
the Iron Age. Possible applications include spatial analysis.

This package provides two datasets:

-   `katzas` an import of the original data
-   `katzar` a translated and reshaped version of the data.

Installation
------------

You can install the development version of `katzar` like this

    library(devtools)
    install_gitlab("nehemie/katzar", host = "https://gitlab.com")

Example
-------

### katzas

This is a basic example which shows you how to plot the data-set
`katzas`

    library(sf)
    #> Linking to GEOS 3.10.0, GDAL 3.3.3, PROJ 8.2.0
    library(katzar)

    ## basic example code: elevation
    data(katzas)
    hist(as.integer(katzas$elevation),
         main = paste("Histogram of" , "sites' elevation above sea level"),
    xlab = ("Elevation in meter")
    )
    title(sub = "(sites are from all periods)")

<img src="man/figures/README-example-katzas-1.png" width="80%" />

### katzar

This is a basic example which shows you how to plot the data-set
`katzar`

    ## Plotting Late Bronze Age site
    data(katzar)

    # Convert elevation
    katzar$elevation <- as.integer(katzar$elevation)
    katzar[is.na(katzar$elevation), "elevation"] <- mean(katzar$elevation, na.rm=TRUE)

    # ggplot
    library(ggplot2)
    ggplot() + geom_sf(data = katzar, aes(colour=elevation)) +
      theme_minimal() +
      theme(plot.caption = element_text(hjust = 1, colour="sienna")) +
      ggtitle("Sites in Sürenhagen's KatzaS database", subtitle ="Coloured by elevation") +
      labs(caption = "Made with katzar", color="Elevation\n(in meter)")

<img src="man/figures/README-example-katzar-1.png" width="80%" />
