#' An R data-package for Sürenhagen's catalog of archaeological sites
#' "KatzaS"
#'
#' The katzar package contains data frames which have been generated
#' based on Dietrich Sürenhagen (Heidelberg). The original dataset is provided via the
#' Hethitologie-Portal Mainz (<hethiter.net/:KatzaS>) and called "Katalog
#' zentralanatolischer Siedlungen des 2. und vorhellenistischen 1.
#' Jahrtausends v. Chr. in neueren Survey-Berichten (KatzaS)". It is
#' available at
#' \href{https://www.hethport.uni-wuerzburg.de/katzas/Katalog.php}{hethiter.net/:KatzaS}:
#' The data frame `katzas` in this package is a direct import into R.
#' The other data frame `katzar`  has undergone translating and tidying
#' so that is they better suited for immediate analysis in R. The
#' purpose of this package is to serve as a central R resource for these
#' data-sets so that they might be used for
#' exploratory data analyses in a replicable manner.
#'
#' Two (non-exported) functions `katzar::katzengraben` and `katzar::hexen` can
#' be used to create the data-set starting from the original webpage. The
#' function `katzengraben` downloads the webpage and creates the
#' data.frame `katzas`. The function `hexen` generates the data.frame
#' `katzar`
#'
#'
#' @section Data frames:
#'
#' The following data frames are loaded for use with the katzar package:
#'
#' \itemize{ \item \code{\link{katzar}} \item \code{\link{katzas}} }
#'
#' @docType package
#' @name The package katzar
NULL
